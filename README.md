# Johns Revisited

My friend (named John) hosted a Minecraft server for my group of friends a while ago, and now control has ceded to me. Here are the associated configs and scripts associated with it (PaperMC, Spigot, Bukkit, EssentialsX, etc.)


# The Update Script

I'm trying to write a bash script to download the HTML file of PaperMC's latest version page, compare it to the last HTML file it downloaded like this. If it is different from the previous HTML, it can be assumed that there was an update to Paper so the script should download the latest paperclip.jar (from that same site). cmp is supposed to have a binary exit status based on whether or not the two files were different, but I cannot get the if-else to work and the script _always_ ends up downloading a new paperclip.jar file. It's not too much of a problem because it's not a big file, but I'm still frustrated.


# To Do:

I still need to actually upload all the config files to this repo, but I've been having a bit of trouble connecting git on my computer to this remote. Time will tell.