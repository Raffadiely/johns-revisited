echo "Checking current saved version..."
touch current.html
touch paperclip.jar
echo "Done."

rm latest.html
wget https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/
mv index.html latest.html


cmp latest.html current.html
if [ "$?" != 0 ]
then
	echo "New version found!"
	echo "Updating local check files..."
	rm current.html
	rm paperclip.jar.old
	mv latest.html current.html
	mv paperclip.jar paperclip.jar.old
	echo "Done."
	echo "Downloading new paperclip..."
	wget https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/paperclip.jar
	echo "Done."
else
	echo "No new version found!"
fi

echo "Starting garbage collection..."
java -Xms6G -Xmx6G -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=100 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:G1MixedGCLiveThresholdPercent=35 -XX:+AlwaysPreTouch -XX:+ParallelRefProcEnabled -Dusing.aikars.flags=mcflags.emc.gs -jar paperclip.jar
echo "Done."
echo "Starting server..."
java -jar paperclip.jar
